<?php

include_once("../vendor/autoload.php");

include_once("../src/Imager.php");
include_once("../src/ImagerException.php");
include_once("../src/ImagerHandlerInterface.php");
include_once("../src/ResizeHandler.php");
include_once("../src/StorageHandler.php");

/**
 * @param React\Http\Request $request
 * @param React\Http\Response $response
 */
$app = function ($request, $response) {
	try {
		$imgr = new \Imager\Imager();
		$imgr->registerHandler("ResizeHandler");
		$imgr->registerHandler("StorageHandler");
		$image = $imgr->processHandlers($request->getPath());

		$response->writeHead(200, array('Content-Type' => $image->getImageFormat()));
		$response->end((string)$image);
	} catch (Exception $e) {
		$response->writeHead(200, array('Content-Type' => 'text/plain'));
		$response->end($e->getMessage());
	}
};

$loop = React\EventLoop\Factory::create();
$socket = new React\Socket\Server($loop);
$http = new React\Http\Server($socket);

$http->on('request', $app);
echo "Server running at http://127.0.0.1:13099\n";

$socket->listen(13099);
$loop->run();
