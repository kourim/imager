<?php
namespace Imager\Handler;

use Imager\Exception\ImagerException;
use Imagick;

class ResizeHandler implements ImagerHandlerInterface
{

	const HANDLER_KEYWORD = 'resize';

	private $width;
	private $height;

	public static function getKeyword()
	{
		return self::HANDLER_KEYWORD;
	}

	public function isLastFilter()
	{
		return false;
	}

	public function getParams(&$path)
	{
		$param = array_shift($path);
		if (preg_match('/^(\d+)x(\d+)$/', $param, $match)) {
			$this->width = $match[1];
			$this->height = $match[2];
		} else {
			array_unshift($path, $param);
			throw new ImagerException('Parameter '.$param.' doesnt fit into resize handler.');
		}
	}

	public function process(Imagick &$image)
	{
		$width = $image->getImageWidth();
		$height = $image->getImageHeight();

		if ($width > $height) {
			$newHeight = $this->height;
			$newWidth = ($this->height / $height) * $width;
		} else {
			$newWidth = $this->width;
			$newHeight = ($this->width / $width) * $height;
		}

		$image->resizeImage($newWidth, $newHeight, Imagick::FILTER_LANCZOS, 0.9, true);
	}

}
