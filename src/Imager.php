<?php
namespace Imager;

use Imager\Exception\ImagerException;
use Imager\Handler\ImagerHandlerInterface;
use Imagick;

class Imager
{

	const APP_CONST = 'IMAGER';

	/**
	 * keep all handlers names identified by keywords
	 * @var ImagerHandlerInterface[] $handlers
	 */
	private $handlers = [];

	/**
	 * queue for all prepared handlers
	 * @var ImagerHandlerInterface[] $queue
	 */
	private $queue = [];

	/**
	 * path to image
	 * @var string $imagePath
	 */
	private $imagePath;

	/**
	 * register handler class under own keyword
	 * @param string $handlerName
	 * @throws ImagerException
	 */
	public function registerHandler($handlerName)
	{
		// we need to fill namespace
		if (strpos($handlerName, "\\Imager\\Handler\\") === false) {
			$handlerName = "\\Imager\\Handler\\".$handlerName;
		}

		// check if class really exists
		if (!class_exists($handlerName)) {
			throw new ImagerException("Handler ".$handlerName." doesnt exist.");
		}

		// check if class realy implement image handler interface
		$interfaces = class_implements($handlerName);
		if (!array_key_exists('Imager\Handler\ImagerHandlerInterface', $interfaces)) {
			throw new ImagerException("Handler ".$handlerName." its not valid Imager handler.");
		}

		// save class for later use
		/** @var $handlerName ImagerHandlerInterface */
		$this->handlers[$handlerName::getKeyword()] = $handlerName;
	}

	/**
	 * create instance of handler
	 * @param string $handlerKeyword
	 * @return ImagerHandlerInterface
	 */
	private function createHandler($handlerKeyword)
	{
		/** @var ImagerHandlerInterface $handler */
		$handler = new $this->handlers[$handlerKeyword]();
		return $handler;
	}

	/**
	 * process image through all handlers
	 * @param string $path
	 * @throws ImagerException
	 * @return Imagick
	 */
	public function processHandlers($path)
	{
		// clear path from empty spaces
		$splittedPath = array_filter(explode('/', $path));

		// trying to find all keywords and parameters in path
		while (!empty($splittedPath)) {
			$keyword = array_shift($splittedPath);
			// if handler exists, create new one and get parameters for him
			if (array_key_exists($keyword, $this->handlers)) {
				try {
					$handler = $this->createHandler($keyword);
					$handler->getParams($splittedPath);
					$this->queue[] = $handler;

					// if this filter should be last
					if ($handler->isLastFilter()) {
						break;
					}

					continue;
				} catch (ImagerException $e) {
					// parameters dont match, skip this handler and try to work with what we have
					array_unshift($splittedPath, $keyword);
					break;
				}
			// if not, its end, rest is path to file
			} else {
				array_unshift($splittedPath, $keyword);
				break;
			}
		}

		// create image object
		try {
			$this->imagePath = implode('/', $splittedPath);
			$image = new Imagick($this->imagePath);
		} catch (\ImagickException $e) {
			throw new ImagerException('Cannot load image '.$this->imagePath);
		}

		// process image object
		/** @var ImagerHandlerInterface $handler */
		foreach ($this->queue as $handler) {
			$handler->process($image);
		}

		return $image;
	}

}