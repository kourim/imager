<?php
namespace Imager\Handler;

use Imagick;

interface ImagerHandlerInterface
{

	/**
	 * keeps keyword for image handler
	 * @return string
	 */
	public static function getKeyword();

	/**
	 * force filter to be the last one
	 * @return bool
	 */
	public function isLastFilter();

	/**
	 * parse params from url input, takes what need and return rest
	 * @param string $path
	 */
	public function getParams(&$path);

	/**
	 * process filter in input image
	 * @param Imagick $image
	 */
	public function process(Imagick &$image);

}
