<?php
namespace Imager\Handler;

use Imager\Exception\ImagerException;
use Imager\Imager;
use Imagick;

class StorageHandler implements ImagerHandlerInterface
{

	const HANDLER_KEYWORD = 'storage';

	const CHANGE_RESIZE = 'resize';
	const CHANGE_CROP = 'crop';

	/**
	 * @var int $width
	 */
	private $width;

	/**
	 * @var int $height
	 */
	private $height;

	/**
	 * @var string $imageHash
	 */
	private $imageHash;

	/**
	 * @var \Predis\Client $redis
	 */
	private static $redis;

	public static function getKeyword()
	{
		return self::HANDLER_KEYWORD;
	}

	public function __construct()
	{
		// connect to redis
	}

	public function isLastFilter()
	{
		return true;
	}

	public function getParams(&$path)
	{
		// pop last element, its filename with height and width
		$imageName = array_pop($path);

		// find height and width in filename
		if (preg_match('/^(\d+)x(\d+)_(.+)$/', $imageName, $match)) {
			$this->width = $match[1];
			$this->height = $match[2];

			// return original keyword, because keyword is in path
			array_unshift($path, self::HANDLER_KEYWORD);
			// return filename to path
			$path[] = $match[3];
		} else {
			throw new ImagerException('Cannot find width and height in storage handler.');
		}
	}

	public function process(Imagick &$image)
	{
		// default background color
		$color = 'rgb(255, 255, 255)';
		$image->setBackgroundColor($color);

		switch ($this->resizeOrCrop()) {
			case self::CHANGE_RESIZE:
				// resize image
				$image->thumbnailImage($this->width, $this->height, true);

				// make border around image to fill empty space
				$borderHeight = 0;
				$borderWidth = 0;
				if ($image->getImageWidth() < $this->width) {
					$borderWidth = floor(($this->width - $image->getImageWidth()) / 2);
				}
				if ($image->getImageHeight() < $this->height) {
					$borderHeight = floor(($this->height - $image->getImageHeight()) / 2);
				}
				if ($borderWidth > 0 || $borderHeight > 0) {
					$image->borderImage($color, $borderWidth, $borderHeight);
				}
				break;
			case self::CHANGE_CROP:
			default:
				$image->cropThumbnailImage($this->width, $this->height);
		}

		// recolor image
		$image->modulateImage(100, 125, 100);
		$image->setImageCompressionQuality(0.8);
	}

	/**
	 * try to figure out, whats best for image, crop or resize
	 * @return string
	 */
	private function resizeOrCrop()
	{
		if (self::$redis) {
			$result = self::$redis->hget(Imager::APP_CONST, $this->imageHash);
			if ($result && $result == self::CHANGE_RESIZE) {
				return self::CHANGE_RESIZE;
			}
		}

		return self::CHANGE_CROP;
	}

}
