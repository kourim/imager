<?php
namespace Imager;

use Exception;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

	private static $config = [];

	public static $self = null;

	/**
	 * return value of parameter named as $key
	 * @param string $key
	 * @return mixed
	 * @throws Exception
	 */
	public static function get($key)
	{
		if (array_key_exists($key, self::$config)) {
			return self::$config[$key];
		}

		throw new Exception('Parameter '.$key.' doesnt exist.');
	}

	/**
	 * Configuration constructor
	 */
	public function __construct()
	{
		self::$self = $this;
	}

	/**
	 * @return TreeBuilder
	 */
	public function getConfigTreeBuilder()
	{
		$treeBuilder = new TreeBuilder();
		$rootNode = $treeBuilder->root('application');
		$rootNode->children()
			->scalarNode('name')
				->isRequired()
				->cannotBeEmpty()
				->end()
			->scalarNode('path')
				->isRequired()
			->end();
		return $treeBuilder;
	}

	/**
	 * save parsed configuration from symfony parser
	 * @param array $config
	 */
	public function saveConfiguration(array $config)
	{
		self::$config = $config;
	}

}